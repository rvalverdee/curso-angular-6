import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destinoViaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';
import { Observable } from 'rxjs';
import { fromEvent } from 'rxjs/observable/fromEvent';
//import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
//import {AppState} from './../app.module';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
//import { ajax } from 'rxjs/ajax';
//import {ElegidoFavoritoAction, NuevoDestinoAction} from './../models/destinos-viajes-state-model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers : [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[]; //variable global le decimos que es una lista del tipo de la clase
  update: string[] ;
  update2 = [];
  searchResults: String[];
  constructor(private destinosapiclient : DestinosApiClient/*, private store: Store<AppState>*/) { 
  	this.destinos = []; //se inicializa la clase vacia
    this.update = [];
    this.update2 = ['Barcelona','Madrid','Sa'];
    /*this.store.select(state => state.destinos.favorito)
    this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log("destinos sub store");
        console.log(data);
        this.destinos = data.items;
      });
    this.store
      .subscribe((data) => {
        console.log("all store");
        console.log(data);
      });
*/
    /*this.destinosapiclient.subscribeOnChange((d:DestinoViaje) =>{
      if(d != null){
        this.update.push ('Se ha elegido ha ' + d.nombre); // agregamos a la lista de updates la subscripicion
      }
    });*/
  }

  ngOnInit() {


    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre,'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      //filter(text => text.length > 4),
      debounceTime(2000),
      distinctUntilChanged(),
      switchMap(() => this.update2
        )
      ).subscribe(ajaxResponse =>{
        this.searchResults = this.update2;

      });
      console.log(this.update2);

    }
    guardar(d: DestinoViaje):boolean{
      this.destinosapiclient.agregarDestino(new DestinoViaje(d.nombre,d.url)); //nuestra lista this le agregamos un objeto de tipo destinos
      this.destinos.push(new DestinoViaje(d.nombre,d.url));
      console.log(this.destinos);
      console.log('update' + this.update);
     // this.store.dispatch(new NuevoDestinoAction(d));
      return false;
    }

    elegido(d: DestinoViaje){
      this.destinos.forEach(function(x){x.setSelected(false)})
      d.setSelected(true);
      //this.store.dispatch(new ElegidoFavoritoAction(d));
    }

  }

