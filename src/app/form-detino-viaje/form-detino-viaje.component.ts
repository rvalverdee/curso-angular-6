import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destinoViaje.model';
import {FormGroup,ReactiveFormsModule,FormBuilder,Validators,FormControl,ValidatorFn} from '@angular/forms';
@Component({
	selector: 'app-form-detino-viaje',
	templateUrl: './form-detino-viaje.component.html',
	styleUrls: ['./form-detino-viaje.component.css']
})
export class FormDetinoViajeComponent implements OnInit {
	@Output() onItemAdded: EventEmitter<DestinoViaje>; 
	fg: FormGroup; 
	minLogitud: number = 4;
	constructor(fb: FormBuilder) {
		this.onItemAdded = new EventEmitter();
		this.fg = fb.group({
			nombre: ['', Validators.compose([Validators.required, this.nombreValidador,this.nombreValidadorParametrizable(this.minLogitud)])],
			url: ['']
		})
		this.fg.valueChanges.subscribe((form: any)=>{
			console.log('Cambio en formulario: ', form);
		});
	}
	guardar(nombre: string, url: string): boolean {
		let d = new DestinoViaje(nombre,url);
		this.onItemAdded.emit(d);
		return false;
	}

	nombreValidador(control: FormControl): { [s:string]: boolean}{
		const l = control.value.toString().trim().length;
		if(l > 0 && l < 5){
			return {'invalidNombre': true};
		}
		return null;
	}
	nombreValidadorParametrizable(minLog: number): ValidatorFn {
		return (control: FormControl) : {[s:string]:boolean} | null => {
			const l = control.value.toString().trim().length;
			if(l > 0 && l < minLog ){
				return {'minLogNombre': true};
			}
			return null;
		}
	}
	ngOnInit() {
	}

}
