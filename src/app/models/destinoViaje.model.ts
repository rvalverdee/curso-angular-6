export class DestinoViaje {
	
	private selected: boolean;
	comodidades : string[];


	constructor(public nombre: string, public url: string){

		this.comodidades = ["Camas grandes", "Agua potable", "Wifi"];
	}

	isSelected(d:boolean): boolean {
		return this.selected;

	}
	setSelected(d:boolean): boolean{
		return this.selected = d;
	}

}