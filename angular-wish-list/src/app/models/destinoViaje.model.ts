import {v4 as uuid} from 'uuid';

export class DestinoViaje {
	
	private selected: boolean;
	comodidades : string[];
	

	servicios: string[];
	id = uuid();
	

	constructor(public nombre: string, public url: string, public votes: number = 0){

		this.comodidades = ["Camas grandes", "Agua potable", "Wifi"];
	}

	isSelected(d:boolean): boolean {
		return this.selected;

	}
	setSelected(d:boolean): boolean{
		return this.selected = d;
	}
	voteUp(){
		this.votes++;
	}
	voteDown(){
		this.votes--;	

	}

}