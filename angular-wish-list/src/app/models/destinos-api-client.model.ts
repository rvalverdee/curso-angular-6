import {Injectable, Inject, forwardRef} from '@angular/core';
import { DestinoViaje } from './../models/destinoViaje.model';
import {Subject, BehaviorSubject,Observable} from 'rxjs';
import { HttpHeaders, HttpRequest, HttpResponse, HttpClient } from '@angular/common/http';
import { NuevoDestinoAction } from './destinos-viajes-state-model';
import { AppState, AppConfig, APP_CONFIG, db } from '../app.module';
import { Store } from '@ngrx/store';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[];
  current: Subject <DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
  private destinos$ = new Subject<DestinoViaje>();

  constructor (    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient)
    {

    this.destinos = [];
  } 
  getById(id: String): DestinoViaje {
    return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
  }

  add(d: DestinoViaje) {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then(destinos => console.log(destinos))
      }
    });
  }
  
  agregarDestino (d:DestinoViaje){
    this.destinos.push(d);
  }
  getAll():DestinoViaje[]{
    return this.destinos;

  }
  elegir (d: DestinoViaje){
    this.destinos.forEach(x=> x.setSelected(false))
    d.setSelected(true);
    this.current.next(d);
  }

  subscribeOnChange (fn){
    this.current.subscribe(fn);

  }
}