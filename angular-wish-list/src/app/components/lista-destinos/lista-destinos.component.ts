import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../../models/destinoViaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Observable } from 'rxjs';
import { fromEvent } from 'rxjs/Observable/fromEvent';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import {AppState} from './../../app.module';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
//import { ajax } from 'rxjs/ajax';
import {ElegidoFavoritoAction, NuevoDestinoAction} from './../../models/destinos-viajes-state-model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers : [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[]; //variable global le decimos que es una lista del tipo de la clase
  update: string[] ;
  update2 = [];
  searchResults: String[];
  constructor(private destinosapiclient : DestinosApiClient, private store: Store<AppState>) { 
    this.destinos = []; //se inicializa la clase vacia
    this.update = [];
    this.update2 = ['Barcelona','Madrid','Sa'];
    }

    ngOnInit() {

      this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const f = data;
        if (f != null) {
          this.update.push('Se eligió: ' + f.nombre);
        }
      });      

    }
    guardar(d: DestinoViaje):boolean{
      this.destinosapiclient.agregarDestino(new DestinoViaje(d.nombre,d.url)); //nuestra lista this le agregamos un objeto de tipo destinos
      this.destinos.push(new DestinoViaje(d.nombre,d.url));
      this.destinosapiclient.add(d);
      console.log(this.destinos);
      console.log('update' + this.update);
      this.store.dispatch(new NuevoDestinoAction(d));
      return false;
    }

    elegido(d: DestinoViaje){
      this.destinos.forEach(function(x){x.setSelected(false)})
      d.setSelected(true);
      this.store.dispatch(new ElegidoFavoritoAction(d));
    }

  }

