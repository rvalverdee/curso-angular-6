import { AngularHolaMundoPage } from './app.po';

describe('angular-hola-mundo App', function() {
  let page: AngularHolaMundoPage;

  beforeEach(() => {
    page = new AngularHolaMundoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
